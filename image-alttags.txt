Array
(
    [0] => Array
        (
            [0] => /play2win-fr/7bar.jpg
            [1] => /play2win-fr/baccarat.jpg
            [2] => /play2win-fr/blackjack.jpg
            [3] => /lg/7bar.jpg
            [4] => /lg/baccarat.jpg
            [5] => /lg/blackjack.jpg
            [6] => /lg/lobby.jpg
            [7] => /lg/roulette.jpg
            [8] => /lg/snow-wonder.jpg
            [9] => /play2win-fr/lobby.jpg
            [10] => /play2win-fr/roulette.jpg
            [11] => /play2win-fr/snow-wonder.jpg
        )

    [1] => Array
        (
            [0] => play2win-fr
            [1] => play2win-fr
            [2] => play2win-fr
            [3] => lg
            [4] => lg
            [5] => lg
            [6] => lg
            [7] => lg
            [8] => lg
            [9] => play2win-fr
            [10] => play2win-fr
            [11] => play2win-fr
        )

    [2] => Array
        (
            [0] => jpg
            [1] => jpg
            [2] => jpg
            [3] => jpg
            [4] => jpg
            [5] => jpg
            [6] => jpg
            [7] => jpg
            [8] => jpg
            [9] => jpg
            [10] => jpg
            [11] => jpg
        )

)


// Screenshots: [DIR]/[IMAGE], dir is referring to the thumbs, width: 442px and 130px
$sv['screenshotsPath'] = 'play2win-fr';
$sv['screenshots'] = array(
	array(
		'filename' => '7bar.jpg',
		'alt' => 'Play2win : Capture d'&eacute;cran de 7bar'
	),
	array(
		'filename' => 'baccarat.jpg',
		'alt' => 'Jeu de Baccarat sur Play2win'
	),
	array(
		'filename' => 'blackjack.jpg',
		'alt' => 'Play2win : Capture d'&eacute;cran de Blackjack'
	),
	array(
		'filename' => '7bar.jpg',
		'alt' => '7bar skjermdump fra Lg'
	),
	array(
		'filename' => 'baccarat.jpg',
		'alt' => 'Baccarat spill p&aring; Lg'
	),
);


// Screenshots: [DIR]/[IMAGE], dir is referring to the thumbs, width: 442px and 130px
$sv['screenshotsPath'] = 'lg';
$sv['screenshots'] = array(
	array(
		'filename' => 'blackjack.jpg',
		'alt' => 'Spilleksempel - Blackjack'
	),
	array(
		'filename' => 'lobby.jpg',
		'alt' => 'Lobby p&aring; Lg'
	),
	array(
		'filename' => 'roulette.jpg',
		'alt' => 'Roulette skjermdump - Lg'
	),
	array(
		'filename' => 'snow-wonder.jpg',
		'alt' => 'Spill Snow wonder p&aring; Lg'
	),
	array(
		'filename' => 'lobby.jpg',
		'alt' => 'Lobby du Play2win'
	),
);


// Screenshots: [DIR]/[IMAGE], dir is referring to the thumbs, width: 442px and 130px
$sv['screenshotsPath'] = 'play2win-fr';
$sv['screenshots'] = array(
	array(
		'filename' => 'roulette.jpg',
		'alt' => 'Capture d'&eacute;cran de Roulette sur Play2win'
	),
	array(
		'filename' => 'snow-wonder.jpg',
		'alt' => 'Aper&ccedil;u de Snow wonder - Play2win'
	),
