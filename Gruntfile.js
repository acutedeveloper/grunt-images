module.exports = function (grunt) {

	grunt.initConfig({
		responsive_images: {
			logos:{
				options: {
					sizes: [{
						width: 146,
						height: 80,
					}],
					engine: 'im'
				},
				files: [{
					expand: true,
					src: ['**/*.{jpg,gif,png}'],
					cwd: 'FPL-Images/',
					custom_dest: 'resized/FPL-Images/{%= path %}/'					
				}]
			},
			thumbnails:{
				options: {
					sizes: [{
						width: 153,
						height: 114
					}],
					engine: 'im',
					// To allow padding on a the image					
					customOut: [
						'-background','none','-gravity', 'North', '-extent', '153x114'
					]
				},
				files: [{
					expand: true,
					src: ['**/*.{jpg,gif,png}'],
					cwd: 'FPL-Images/',
					custom_dest: 'resized/{%= path %}/'					
				}]
			},
			screenshots: {
				options: {
					sizes: [{
						width: 800,
						height: 600,
						name: "lg"
					}],
					engine: 'im',
					// To allow padding on a the image
					customOut: [
						'-background','none','-gravity', 'North', '-extent', '800x600'
					]
				},
				files: [{
					expand: true,
					src: ['**/*.{jpg,gif,png}'],
					cwd: 'FPL-Images/',
					custom_dest: 'resized/{%= path %}/{%= name %}'
				}]
			},
		},
		kraken: {
			options: {
				key: 'bb797dbc18c0f64c89c7ca4630a3f115',
				secret: '71e9d60a2228fdc9aaec7fc93ebbeb5655bd50be',
				lossy: true
			},
			dynamic: {
				files: [{
					expand: true,
					cwd: 'resized/',
					src: ['**/*.{gif,jpg,jpeg,png}'],
					dest: 'resized/'
				}]
			}
		}

	});

	grunt.loadNpmTasks('grunt-responsive-images');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-imagemagick');
	grunt.loadNpmTasks('grunt-contrib-kraken');
	grunt.registerTask('screenshots', ['responsive_images:thumbnails', 'responsive_images:screenshots', 'kraken']);
	grunt.registerTask('logos', ['responsive_images:logos', 'kraken']);
	//grunt.registerTask('singleimage', ['copy:fullsize', 'imagemagick-resize']);
};