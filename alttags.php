<?php
$output = shell_exec('tree -if resized');

preg_match_all('/\/([a-z-0-9]{1,})\/[A-Za-z-0-9\s]{1,}\.(png|jpg)/', $output, $images_array);

$i = 0;
$languages = array('en', 'dk', 'fr', 'de', 'it', 'nl', 'no', 'pt', 'es', 'sv', 'po');

print_r($images_array);

foreach($images_array[0] as $images){


    $images = explode('/', $images);
    //print_r($images);
    $casinoname = $images[1];
    $screenshot_path = $images[1];
    $screenshot = $images[2];

    // Get the language
    $folder_array = explode('-', $images_array[1][$i]);
    $lang = $folder_array[count($folder_array) - 1];

    if(in_array($lang, $languages)){
        $lang = strtoupper($lang);
        array_pop($folder_array);
        $casinoname = implode('-', $folder_array);
    } else {
        $lang = 'NO';
    }

    $alttag = build_alt_tag($lang, $casinoname, preg_replace('/\.(png|jpg)/', '', $screenshot));

    // Open the array
    if($i %5 == 0)
    {
        echo "\n\n// Screenshots: [DIR]/[IMAGE], dir is referring to the thumbs, width: 442px and 130px\n";
        echo "\$sv['screenshotsPath'] = '".$screenshot_path."';\n";
        echo "\$sv['screenshots'] = array(\n";
    }

    // Fill the array
    echo "\tarray(\n";
    echo "\t\t'filename' => '".$screenshot."',\n";
    echo "\t\t'alt' => '".$alttag."'\n";
    echo "\t),\n";

    // Increase the count first
    $i++;

    //Close the array
    if($i %5 == 0)
    {
        echo ");\n";
    }

}


function build_alt_tag($lang, $casinoname, $screenshot)
{

    // Check for lobby or homepage in $screenshot

    // Clean up the screenshot namespace
    $pattern = '/[0-9A-Za-z-]{0,}\/([a-zA-Z_\-0-9\s]{0,})\.[a-zA-Z]{0,5}/';
    $replacement = '${1}';
    $screenshot = preg_replace($pattern, $replacement, str_replace(array('-', '_'), ' ', $screenshot));
    $casinoname = ucwords(str_replace('-', ' ', $casinoname));

    // Check for CAFR
    $lang = ($lang == 'CAFR' ? 'FR' : $lang);


    if (preg_match('/(hp(?!ark)|home|lobby)/', $screenshot)) {
        $screenshot = ( preg_match('/(hp|home)/', $screenshot ) ? 'hp' : 'lobby' );

        $screenshot_alt = array(
            'EN' => array(
                'hp' => 'Homepage of %1$s',
                'lobby' => 'Lobby at %1$s'
            ),
            'DK' => array(
                'hp' => 'Hjem p&aring; %1$s',
                'lobby' => 'Lobby p&aring; %1$s'
            ),
            'FR' => array(
                'hp' => 'Page d\'accueil du %1$s',
                'lobby' => 'Lobby du %1$s'
            ),
            'DE' => array(
                'hp' => '%1$s Startseite',
                'lobby' => '%1$s Lobby'
            ),
            'IT' => array(
                'hp' => 'Home di %1$s',
                'lobby' => 'Lobby di %1$s'
            ),
            'NL' => array(
                'hp' => 'De Homepagina van %1$s',
                'lobby' => 'De Lobby van %1$s'
            ),
            'NO' => array(
                'hp' => 'Hjem p&aring; %1$s',
                'lobby' => 'Lobby p&aring; %1$s'
            ),
            'PT' => array(
                'hp' => 'P&aacute;gina incicial do %1$s',
                'lobby' => 'Lobby do %1$s'
            ),
            'ES' => array(
                'hp' => 'P&aacute;gina de inicio de %1$s',
                'lobby' => 'Sala de %1$s'
            ),
            'SV' => array(
                'hp' => 'Hem p&aring; %1$s',
                'lobby' => 'Lobby p&aring; %1$s'
            ),
            'PO' => array(
                'hp' => 'Hem p&aring; %1$s',
                'lobby' => 'Zrzut ekranu %1$s'
            ),
        );

        return sprintf($screenshot_alt[$lang][$screenshot], $casinoname);
    } else {
        $alt_tag_strings = array(
            'EN' => array(
                '%1$s %2$s',
                '%2$s at %1$s',
                '%2$s Gameplay at %1$s',
                '%2$s Screenshot - %1$s',
                '%2$s Screenshot at %1$s',
                '%2$s Play at %1$s',
                '%2$s Gameplay',
                '%2$s View - %1$s',
                'Playing %2$s',
                'Playing %2$s at %1$s',
                'Playing %2$s - %1$s',
                'Screenshot of %1$s %2$s',
                'Screenshot of %2$s Gameplay',
                'Screenshot of %2$s Gameplay at %1$s',
                'In-Game Play - %2$s',
                'In-Game Play - %2$s at %1$s',
            ),
            'DK' => array(
                '%1$s %2$s',
                '%2$s p&aring; %1$s',
                '%2$s spiloplevelse p&aring; %1$s',
                '%2$s Screenshot - %1$s',
                '%2$s Screenshot fra %1$s',
                '%2$s spil p&aring; %1$s',
                '%2$s spiloplevelse',
                '%2$s uddrag - %1$s',
                'Spil %2$s',
                'Spil %2$s p&aring; %1$s',
                'Spil %2$s - %1$s',
                'Screenshot af %1$s %2$s',
                'Screenshot af %2$s spiloplevelse',
                'Screenshot af %2$s spiloplevelse p&aring; %1$s',
                'Spileksempel - %2$s',
                'Spileksempel - %2$s p&aring; %1$s',
            ),
            'FR' => array(
                '%1$s %2$s',
                '%2$s sur %1$s',
                'Fonctionnalit&eacute;s de %2$s sur %1$s',
                'Capture d\'&eacute;cran de %2$s - %1$s',
                '%2$s : Capture d\'&eacute;cran sur %1$s',
                '%2$s : jeu de %1$s',
                'Fonctionnalit&eacute;s de %2$s',
                'Aper&ccedil;u de %2$s - %1$s',
                'Jeu de %2$s',
                'Jeu de %2$s sur %1$s',
                'Jeu de %2$s - %1$s',
                '%1$s : Capture d\'&eacute;cran de %2$s',
                'Capture d\'&eacute;cran de %2$s',
                'Capture d\'&eacute;cran de %2$s sur %1$s',
                'Aper&ccedil;u - %2$s',
                'Aper&ccedil;u - %2$s sur %1$s',
            ),
            'DE' => array(
                '%1$s %2$s',
                '%2$s bei %1$s',
                '%2$s Spielmechanik bei %1$s',
                '%2$s Screenshot - %1$s',
                '%2$s Screenshot bei %1$s',
                '%2$s bei %1$s',
                '%2$s Spielmechanik',
                '%2$s ansehen - %1$s',
                '%2$s spielen',
                '%2$s bei %1$s spielen',
                '%2$s spielen - %1$s',
                'Screenshot von %1$s %2$s',
                'Screenshot der %2$s Spielmechanik',
                'Screenshot der %2$s Spielmechanik bei %1$s',
                'In-Game Spiel - %2$s',
                'In-Game Spiel - %2$s bei %1$s',
            ),
            'IT' => array(
                '%1$s %2$s',
                '%2$s di %1$s',
                'Modalit&agrave; di gioco di %2$s su %1$s',
                'Schermata di %2$s - %1$s',
                'Schermata di %2$s su %1$s',
                'Gioca a %2$s su %1$s',
                '%2$s - Modalit&agrave; di gioco',
                'Anteprima di %2$s - %1$s',
                'Partita a %2$s',
                'Partita a %2$s su %1$s',
                'Partita a %2$s - %1$s',
                'Schermata di %1$s %2$s',
                'Schermata di gioco di %2$s',
                'Schermata di gioco di %2$s su %1$s',
                'Gioco - %2$s',
                'Gioco - %2$s su %1$s',
            ),
            'NL' => array(
                '%1$s %2$s',
                '%2$s bij %1$s',
                '%2$s-Gameplay bij %1$s',
                '%2$s-Schermafbeelding - %1$s',
                '%2$s-Schermafbeelding bij %1$s',
                '%2$s Speel bij %1$s',
                '%2$s-Gameplay',
                '%2$s Bekijk %1$s',
                '%2$s aan het spelen - ',
                'Playing %2$s  aan het spelen bij %1$s',
                'Playing %2$s aan het spelen - %1$s',
                'Schermafbeelding van %1$s %2$s',
                'Schermafbeelding van %2$s-Gameplay',
                'Schermafbeelding van %2$s-Gameplay bij %1$s',
                'In-Game  spelverloop - %2$s',
                'In-Game  spelverloop - %2$s bij %1$s',
            ),
            'NO' => array(
                '%1$s %2$s',
                '%2$s p&aring; %1$s',
                '%2$s spillopplevelse p&aring; %1$s',
                '%2$s skjermdump - %1$s',
                '%2$s skjermdump fra %1$s',
                '%2$s spill p&aring; %1$s',
                '%2$s spillopplevelse',
                '%2$s utdrag - %1$s',
                'Spill %2$s',
                'Spill %2$s p&aring; %1$s',
                'Spill %2$s - %1$s',
                'Skjermdump av %1$s %2$s',
                'Skjermdump av %2$s spillopplevelse',
                'Skjermdump av %2$s spillopplevelse p&aring; %1$s',
                'Spilleksempel - %2$s',
                'Spilleksempel - %2$s p&aring; %1$s',
            ),
            'PT' => array(
                '%1$s %2$s',
                '%2$s no %1$s',
                'Jogabilidade de %2$s no %1$s',
                'Captura de tela %2$s - %1$s',
                'Captura de tela %2$s no %1$s',
                'Jogar %2$s no %1$s',
                'Jogabilidade de %2$s',
                'Ver %2$s - %1$s',
                'Jogando %2$s',
                'Jogando %2$s no %1$s',
                'Jogando %2$s - %1$s',
                'Captura de tela do %1$s %2$s',
                'Captura de tela da jogabilidade de %2$s',
                'Captura de tela da jogabilidade de  %2$s no %1$s',
                'Jogo in-game - %2$s',
                'Jogo in-game - %2$s no %1$s',
            ),
            'ES' => array(
                '%1$s %2$s',
                '%2$s en %1$s',
                'Sistema de juego de %2$s en %1$s',
                'Captura de pantalla de %2$s - %1$s',
                'Captura de pantalla de %2$s en %1$s',
                'Juega a %2$s en %1$s',
                'Sistema de juego de %2$s',
                'Vista de View - %1$s',
                'Jugando a %2$s',
                'Jugando a %2$s en %1$s',
                'Jugando a %2$s - %1$s',
                'Captura de pantalla de %1$s %2$s',
                'Captura de pantalla del sistema de juego de %2$s',
                'Captura de pantalla del sistema de juego de %2$s en %1$s',
                'Experiencia de juego - %2$s',
                'Experiencia de juego - %2$s en %1$s',
            ),
            'SV' => array(
                '%1$s %2$s',
                '%2$s p&aring; %1$s',
                '%2$s spelupplevelse p&aring; %1$s',
                '%2$s sk&auml;rmbild - %1$s',
                '%2$s sk&auml;rmbild fr&aring;n %1$s',
                '%2$s spela p&aring; %1$s',
                '%2$s spelupplevelse',
                '%2$s utdrag - %1$s',
                'Spela %2$s',
                'Spela %2$s p&aring; %1$s',
                'Spela %2$s - %1$s',
                'Sk&auml;rmbild av %1$s %2$s',
                'Sk&auml;rmbild av %2$s spelupplevelse',
                'Sk&auml;rmbild av %2$s spelupplevelse p&aring; %1$s',
                'Spelexempel - %2$s',
                'Spelexempel - %2$s p&aring; %1$s',
            ),
            'PO' => array(
                '%1$s w %2$s',
                'Gra %1$s w %2$s',
                'Zrzut ekranu %1$s - %2$s',
                'Zrzut ekranu %1$s w %2$s',
                'Graj %1$s w %2$s',
                'Gra %1$s',
                'Podgl&aogon;d %1$s - %2$s',
                'Rozpocz&eogon;cie gry %1$s',
                'Rozpocz&eogon;cie gry %1$s w %2$s',
                'Rozpocz&eogon;cie gry %1$s - %2$s',
                'Zrzut ekranu %2$s %1$s',
                'Zrzut ekranu gry %1$s',
                'Zrzut ekranu gry %1$s w %2$s',
                'Rozgrywka w grze - %1$s',
                'Rozgrywka w grze - %1$s w %2$s',
            ),
        );

        $rand = rand(0, 14);
        return sprintf($alt_tag_strings[$lang][$rand], $casinoname, ucfirst($screenshot));
    }
}
